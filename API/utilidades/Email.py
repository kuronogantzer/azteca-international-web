from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
import smtplib

def EnviarMensaje(destinatario, mensaje):
    '''Env�a mensajes a correo electr�nico una chimba pap�!'''
    EnviarMensajeRemitente(destinatario, mensaje, "user@mail.com", "password")

def EnviarMensajeRemitente(destinatario, message, remitente, password):
    msg = MIMEMultipart()

    msg['From'] = remitente
    msg['To'] = destinatario
    msg['Subject'] = "Registro exitoso"

    # add in the message body
    msg.attach(MIMEText(message, 'plain'))

    # create server
    server = smtplib.SMTP('smtp.gmail.com: 587')

    server.starttls()

    # Login Credentials for sending the mail
    server.login(msg['From'], password)

    # send the message via the server.
    server.sendmail(msg['From'], msg['To'], msg.as_string())

    server.quit()