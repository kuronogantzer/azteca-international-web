from Modelos import *
# from utilidades.Shortcuts import *
from playhouse.shortcuts import model_to_dict, dict_to_model
import hashlib
import datetime
from utilidades.Email import *
import random
import concurrent.futures
# import socketio
import jwt

class Negocio:

    JWT_SECRET = 'melanoghaster'
    JWT_ALGORITHM = 'HS256'
    JWT_EXP_DELTA_SECONDS = 300
    tokens = []

    def ConsultarEstadosCodigos(self):
        entidad = EstadoCodigo()
        resultados = entidad.select(EstadoCodigo)
        resultados = [model_to_dict(resultado, recurse=True) for resultado in resultados]
        return resultados

    def ConsultarUsuarios(self):
        resultados = Usuario.select(Usuario).where((Usuario.usuario != 'admin') & (Usuario.activo == 1))
        resultados = [model_to_dict(resultado, recurse=True) for resultado in resultados]
        return resultados

    def Login(self, usuario, contrasena, ipOrigen):
        contrasena = hashlib.sha256(contrasena.encode('utf-8')).hexdigest()
        try:
            resultado = Usuario.get(Usuario.contrasena == contrasena, fn.Lower(Usuario.usuario) == usuario.lower())
        except Exception as e:
            print(e)
            return 'El nombre de usuario o la contraseña no son correctos.'
        usuario = model_to_dict(resultado)
        rolesPantallas = RolPantalla.select().join(Rol).where((Rol.id == resultado.rol.id))
        rolesPantallas = [model_to_dict(rolPantalla) for rolPantalla in rolesPantallas]
        token = self.GenerarToken(usuario['id'], ipOrigen)
        resultado = {'usuario': usuario, 'rolesPantallas': rolesPantallas, 'token': token}
        return resultado

    def GenerarToken(self, idUsuario, ipOrigen):
        try:
            self.tokens[:] = [d for d in self.tokens if d.get(idUsuario) is None]
        except:
            print('Generar token completamente nuevo')

        payload = {
            'user_id': idUsuario,
            'ipOrigen': ipOrigen,
            'exp': datetime.datetime.utcnow() + datetime.timedelta(seconds=self.JWT_EXP_DELTA_SECONDS)
        }
        jwt_token = jwt.encode(payload, self.JWT_SECRET, self.JWT_ALGORITHM)
        jwt_token = jwt_token.decode('utf-8')
        self.tokens.append({idUsuario: jwt_token})
        return jwt_token

    def ValidarToken(self, token):
        token = jwt.decode(token, self.JWT_SECRET, algorithms=[self.JWT_ALGORITHM])
        trueToken = [d for d in self.tokens if d.get(token['user_id']) is not None]
        if len(trueToken) is 0:
            raise ValueError('Usuario sin token')
        return token

    def EliminarToken(self, idUsuario):
        try:
            self.tokens.remove(self.tokens[int(idUsuario)])
        except:
            print('El token no existe')

    def NuevoUsuario(self, usuario):
        usuario = dict_to_model(Usuario, usuario, True)
        usuarioExistente = None
        try:
            usuarioExistente = Usuario.get((fn.Lower(Usuario.usuario) == fn.Lower(usuario.usuario)) & (Usuario.activo == 0))
        except:
            print('No existe el mismo usuario ' + usuario.usuario + ' inactivo')
        if usuarioExistente is not None:
            self.ActivarUsuario(usuario, usuarioExistente)
            return True
        validation = Usuario.select().where((fn.Lower(Usuario.usuario) == fn.Lower(usuario.usuario)) & (Usuario.activo == 1)).count()
        if validation > 0:
            return 'Ya existe un usuario con el mismo nombre de usuario: ' + usuario.usuario
        validation = Usuario.select().join(Email, on=Usuario.email == Email.id).where((fn.Lower(Email.email) == fn.Lower(usuario.email.email)) & (Usuario.activo == 1)).count()
        if validation > 0:
            return 'Ya existe un usuario con el mismo email: ' + usuario.email
        validation = Usuario.select().join(TipoIdentificacion).where((Usuario.numero_identificacion == usuario.numero_identificacion)
                                                                     & (TipoIdentificacion.id == usuario.tipo_identificacion)
                                                                     & (Usuario.activo == 1)).count()
        if validation > 0:
            return 'Ya existe un usuario con el mismo número y tipo de identificación'
        contrasena = usuario.contrasena
        usuario.contrasena = hashlib.sha256(usuario.contrasena.encode('utf-8')).hexdigest()
        usuario.fecha_activacion = datetime.datetime.now()
        usuario.fecha_registro = datetime.datetime.now()
        usuario.activo = 1

        try:
            email = Email.get(Email.id == usuario.email.id)
        except:
            return 'El email seleccionado no existe'
        email.asignado = 1
        email.save()
        usuario.save()
        try:
            EnviarMensaje(email.email, "Usuario registrado.\nNombre de usuario: " + usuario.usuario + "\nContraseña de usuario: " + contrasena)
        except:
            return 'Error al enviar email al usuario'
        return True

    def ActualizarUsuario(self, usuario):
        usuario = dict_to_model(Usuario, usuario, True)
        anteriorUsuario = Usuario.get(Usuario.id == usuario.id)
        usuario.contrasena = anteriorUsuario.contrasena
        usuario.fecha_activacion = anteriorUsuario.fecha_activacion
        usuario.fecha_registro = anteriorUsuario.fecha_registro
        usuario.fecha_ultima_modificacion = datetime.datetime.now()
        usuario.save()
        return True

    def InactivarUsuario(self, id):
        usuario = Usuario.get(Usuario.id == id)
        usuario.activo = 0
        usuario.fecha_ultima_modificacion = datetime.datetime.now()
        usuario.save()
        return True

    def ActivarUsuario(self, usuario: Usuario, anteriorUsuario: Usuario):
        usuario.activo = 1
        usuario.fecha_ultima_modificacion = datetime.datetime.now()
        usuario.fecha_activacion = datetime.datetime.now()
        usuario.fecha_registro = anteriorUsuario.fecha_registro
        contrasena = usuario.contrasena
        usuario.contrasena = hashlib.sha256(usuario.contrasena.encode('utf-8')).hexdigest()
        usuario.save()
        EnviarMensaje(usuario.email,
                      "Usuario registrado.\nNombre de usuario: " + usuario.usuario + "\nContraseña de usuario: " + contrasena)

    def VerificarPermisos(self, idUsuario, ruta):
        try:
            usuario = Usuario.get(Usuario.id == idUsuario)
            pantalla = Pantalla.get(Pantalla.ruta == ruta)
        except:
            return 'El usuario o la pantalla no existe'
        rolesPantallas = RolPantalla.select().join(Rol, on=(RolPantalla.rol == Rol.id))\
            .where(Rol.id == usuario.rol.id).join(Pantalla, on=(RolPantalla.pantalla == Pantalla.id))\
            .where(Pantalla.id == pantalla.id)
        rolesPantallas = [model_to_dict(rolPantalla) for rolPantalla in rolesPantallas]
        if len(rolesPantallas) is 0:
            return False
        else:
            return True

    def ConsultarTiposDocumento(self):
        resultados = TipoIdentificacion.select(TipoIdentificacion)
        resultados = [model_to_dict(resultado, True) for resultado in resultados]
        return resultados

    def ConsultarCiudades(self):
        resultados = Ciudad.select(Ciudad)
        resultados = [model_to_dict(resultado, True) for resultado in resultados]
        return resultados

    def ConsultarUsuarioPorId(self, id) -> Usuario:
        resultado = Usuario.get(Usuario.id == id)
        resultado.contrasena = None
        resultado = model_to_dict(resultado, True)
        return resultado

    def ConsultarRoles(self):
        resultados = Rol.select().where(Rol.nombre != 'admin')
        resultados = [model_to_dict(resultado, True) for resultado in resultados]

        return resultados

    def ConsultarRolPorId(self, id) -> Rol:
        try:
            resultado = Rol.get(Rol.id == id)
        except:
            return 'Rol no encontrado'
        resultado = model_to_dict(resultado, True)
        rolPantallas = Pantalla.select().join(RolPantalla).join(Rol).where(Rol.id == id)
        resultado['rolPantallas'] = [model_to_dict(resultado, True) for resultado in rolPantallas]
        return resultado

    def CrearRol(self, rol):
        rolPantallas = [dict_to_model(RolPantalla, rolPantalla, True) for rolPantalla in rol['rolPantallas']]
        rol = dict_to_model(Rol, rol, True)
        validacion = Rol.select().where(fn.Lower(Rol.nombre) == fn.Lower(rol.nombre)).count()
        if validacion > 0:
            return 'Ya existe un rol con el mismo nombre.'
        rol.save()
        for rolPantalla in rolPantallas:
            rolPantalla.rol = rol.id
            rolPantalla.save()
        return True

    def EliminarRol(self, id):
        usuarios = Usuario.select().join(Rol).where(Rol.id == id)
        for usuario in usuarios:
            usuario.rol = None
            usuario.save()
        rolPantallas = RolPantalla.select().join(Rol).where(Rol.id == id)
        for rolPantalla in rolPantallas:
            rolPantalla.delete_instance()
        rol = Rol.get(Rol.id == id)
        rol.delete_instance()
        return True

    def ActualizarRol(self, rol):
        anteriorRolPantallas = RolPantalla.select().join(Rol).where(Rol.id == int(rol['id']))
        rolPantallas = [dict_to_model(RolPantalla, rolPantalla, True) for rolPantalla in rol['rolPantallas']]
        rol = dict_to_model(Rol, rol, True)
        rol.save()
        for anteriorRolPantalla in anteriorRolPantallas:
            anteriorRolPantalla.delete_instance()
        for rolPantalla in rolPantallas:
            rolPantalla.save()
        return True

    def ConsultarPantallas(self):
        resultados = Pantalla.select()
        resultados = [model_to_dict(resultado, True) for resultado in resultados]
        return resultados

    def ConsultarEmails(self):
        resultados = Email.select().where(Email.email != 'admin@skycellular.net')
        resultados = [model_to_dict(resultado, True) for resultado in resultados]
        return resultados

    def ConsultarEmailPorEstado(self, estado:int):
        resultados = Email.select().where(Email.asignado == estado)
        resultados = [model_to_dict(resultado, True) for resultado in resultados]
        return resultados

    def RegistrarEmail(self, email):
        try:
            try:
                consulta = Email.get(Email.email == email['email'])
            except:
                email = dict_to_model(Email, email, True)
                email.save()
                return True
        except:
            return False

    def EliminarEmail(self, idEmail):
        try:
            # email = Email.get(Email.id == idEmail)
            Email.delete().where(Email.id == idEmail).execute()
            return True
        except:
            return False

    def ActualizarEmail(self, email):
        try:
            email = dict_to_model(Email, email, True)
            email.save()
            return True
        except:
            return False

    def CrearConfiguracionLeads(self, configuracionLeads):
        """
        Crea una configuración para la generación de leads
        :param configuracionLeads: Objeto con la información de la configuración
        :return: True si es exitoso, False si falla
        """
        try:
            codigosSeleccionados = [dict_to_model(EstadoCodigo, codigoEstado, True) for codigoEstado in configuracionLeads['estadosCodigos']]
            configuracionLeads = dict_to_model(ConfiguracionLeads, configuracionLeads, True)
            # configuracionLeads.estadosCodigos.add(EstadoCodigo.select().where(EstadoCodigo.id.in_(configuracionLeads)))
            configuracionLeads.save()
            codigos = []
            for codigo in codigosSeleccionados:
                relacion = {
                    'estado_codigo': codigo,
                    'configuracion_leads': configuracionLeads
                }
                # relacion.estado_codigo = codigo
                # relacion.configuracion_leads = configuracionLeads
                codigos.append(relacion)
            ConfiguracionLeadsEstados.insert_many(codigos).execute()
            return True
        except Exception as e:
            print(e)
            return False

    def AcualizarConfiguracionLeads(self, configuracionLeads):
        """
        Actualiza una configuración par ala generación de leads
        :param configuracionLads: Objeto con la información de la configuración
        :return: True si es exitoso, False si falla
        """
        try:
            codigosSeleccionados = [dict_to_model(EstadoCodigo, codigoEstado, True) for codigoEstado in
                                    configuracionLeads['estadosCodigos']]
            configuracionLeads = dict_to_model(ConfiguracionLeads, configuracionLeads, True)
            codigosAnteriores = ConfiguracionLeadsEstados.select().join(ConfiguracionLeads).where(ConfiguracionLeads.id == configuracionLeads.id)
            for codigoAnterior in codigosAnteriores:
                codigoAnterior.delete().execute()
            configuracionLeads.save()
            for codigoSeleccionado in codigosSeleccionados:
                configuracionCodigo = ConfiguracionLeadsEstados()
                configuracionCodigo.estado_codigo = codigoSeleccionado
                configuracionCodigo.configuracion_leads = configuracionLeads
                configuracionCodigo.save()
            return True
        except Exception as e:
            print(e)
            return False

    def ConsultarConfiguracionesLeads(self):
        """
        :return: Una lista con las configuraciones existentes
        Consulta todas las configuraciones de generación de leads
        """
        configuracionesLeads = ConfiguracionLeads.select()
        configuracionesLeads = [model_to_dict(configuracionLeads) for configuracionLeads in configuracionesLeads]
        return configuracionesLeads

    def ConsultarConfiguracionLeads(self, idConfiguracionLeads):
        """
        Consulta una configuración de generación de leads por ID
        :param idConfiguracionLeads: identificador de la configuración consultada
        :return: Objeto con la información de la configuración consultada
        """
        try:
            configuracionLeads = ConfiguracionLeads.get(ConfiguracionLeads.id == idConfiguracionLeads)
            configuracionLeads = model_to_dict(configuracionLeads)
            estadosCodigos = ConfiguracionLeadsEstados.select().join(ConfiguracionLeads).where(ConfiguracionLeads.id == idConfiguracionLeads)
            configuracionLeads['estadosCodigos'] = [model_to_dict(estadoCodigo.estado_codigo) for estadoCodigo in estadosCodigos]
            return configuracionLeads
        except Exception as e:
            print(e)
            return 'La configuración solicitada no existe'

    def EliminarConfiguracionLeads(self, idConfiguracionLeads):
        """
        Elimina la configuración de generación de leads solicitada
        :param idConfiguracionLeads: identificador de la configuración a eliminar
        :return: True si es exitoso, False si falla
        """
        try:
            configuracionEstados = ConfiguracionLeadsEstados.select().join(ConfiguracionLeads).where(ConfiguracionLeads.id == idConfiguracionLeads)
            for configuracionEstado in configuracionEstados:
                configuracionEstado.delete().execute()
            ConfiguracionLeads.delete().where(ConfiguracionLeads.id == idConfiguracionLeads).execute()
            return True
        except Exception as e:
            print(e)
            return False

    def CambiarEstadoConfiguracionLeads(self, idConfiguracionLeads): #, estado):
        """
        Cambiar el estado de la configuracion de generación de leads entre activo e inactivo
        :param idConfiguracionLeads: identificador de la configuración que se desea activa/inactivar
        :param estado: 0 para inactivo y 1 para activo
        :return: True si es exitoso, False si falla
        """
        try:
            configuracionLeads = ConfiguracionLeads.get(ConfiguracionLeads.id == idConfiguracionLeads)
            # configuracionLeads.activa = estado
            configuracionLeads.activa = not configuracionLeads.activa
            configuracionLeads.save()
            return True
        except Exception as e:
            print(e)
            return False

    def GenerarLeads(self, estadosCodigosSeleccionados):
        estadosCodigosSeleccionados = [dict_to_model(EstadoCodigo, estadoCodigo, True) for estadoCodigo in estadosCodigosSeleccionados]
        nuevosTelefonos = []
        with concurrent.futures.ThreadPoolExecutor() as executer:
            for out1 in executer.map(self.GenerarLeadsCodigo, estadosCodigosSeleccionados):
                nuevosTelefonos = nuevosTelefonos + out1
        Telefonos.insert_many(nuevosTelefonos).execute()
        return True

    def GenerarLeadsCodigo(self, estadoCodigoSeleccionado):
        telefonosActuales = Telefonos.select().join(EstadoCodigo).where(EstadoCodigo.id == estadoCodigoSeleccionado.id)
        nuevosTelefonos = set()
        telefonos = []

        for i in range(1000):
            nuevoNumero = random.randrange(1000000, 9999999)
            nuevosTelefonos, telefono = self.GenerarNumero(nuevosTelefonos, nuevoNumero, telefonosActuales,
                                                           estadoCodigoSeleccionado.codigo)
            telefonos.append(telefono)
            # self.generados += 1
            # self.sio.emit('Respuesta', {'data':'hola desde el servidor'}, room=self.sid)
        return telefonos

    def GenerarNumero(self, listaActual: set, nuevoNumero: str, telefonosActuales, codigo: str):
        telefonos = [telefono.numero if telefono.numero_base == nuevoNumero else None for telefono in telefonosActuales]
        if nuevoNumero not in listaActual and not any(telefonos):
            listaActual.add(str(codigo) + str(nuevoNumero))
            telefono = {
                'estado_codigo': EstadoCodigo.get(EstadoCodigo.codigo == codigo),
                'enviado': 0,
                'telefono': str(codigo) + str(nuevoNumero),
                'numero_base': str(nuevoNumero),
                'fecha': datetime.datetime.now()
            }
            return listaActual, telefono
        else:
            nuevoNumero = random.randrange(1000000, 9999999)
            self.GenerarNumero(listaActual, str(nuevoNumero), telefonosActuales, codigo)

from threading import *

class Multiproceso(Thread):

    thread_stop_event = Event()
    estadoCodigoSeleccionado = None

    def __init__(self, estadoCodigoSeleccionado):
        self.delay = 1
        self.progress = 0
        self.estadoCodigoSeleccionado = estadoCodigoSeleccionado
        super(Multiproceso, self).__init__()

    def run(self):
        telefonosActuales = Telefonos.select().join(EstadoCodigo).where(EstadoCodigo.id == self.estadoCodigoSeleccionado.id)
        nuevosTelefonos = set()
        telefonos = []
        for i in range(1000):
            nuevoNumero = random.randrange(1000000, 9999999)
            nuevosTelefonos, telefono = self.GenerarNumero(nuevosTelefonos, nuevoNumero, telefonosActuales,
                                                           self.estadoCodigoSeleccionado.codigo)
            telefonos.append(telefono)
        Telefonos.insert_many(telefonos).execute()

    def GenerarNumero(self, listaActual: set, nuevoNumero: str, telefonosActuales, codigo: str):
        telefonos = [telefono.numero if telefono.numero_base == nuevoNumero else None for telefono in telefonosActuales]
        if nuevoNumero not in listaActual and not any(telefonos):
            listaActual.add(str(codigo) + str(nuevoNumero))
            telefono = {
                'estado_codigo': EstadoCodigo.get(EstadoCodigo.codigo == codigo),
                'enviado': 0,
                'telefono': str(codigo) + str(nuevoNumero),
                'numero_base': str(nuevoNumero),
                'fecha': datetime.datetime.now()
            }
            return listaActual, telefono
        else:
            nuevoNumero = random.randrange(1000000, 9999999)
            self.GenerarNumero(listaActual, str(nuevoNumero), telefonosActuales, codigo)