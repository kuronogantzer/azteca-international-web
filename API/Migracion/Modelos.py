from peewee import *

database = MySQLDatabase('azteca_int', **{'charset': 'utf8', 'use_unicode': True, 'user': 'root', 'password': 'asdf 1234'})

class UnknownField(object):
    def __init__(self, *_, **__): pass

class BaseModel(Model):
    class Meta:
        database = database

class Audio(BaseModel):
    estado = CharField(column_name='ESTADO', null=True)
    fecha_ultima_modificacion = DateTimeField(column_name='FECHA_ULTIMA_MODIFICACION', null=True)
    nombre = CharField(column_name='NOMBRE')
    ubicacion_actual = CharField(column_name='UBICACION_ACTUAL', null=True)

    class Meta:
        table_name = 'audio'
        primary_key = False

class Ciudad(BaseModel):
    departamento = CharField()
    estado = IntegerField()
    nombre = CharField(constraints=[SQL("DEFAULT ''")])

    class Meta:
        table_name = 'ciudad'


class EstadoCodigo(BaseModel):
    ciudad = CharField(column_name='CIUDAD', null=True)
    codigo = CharField(column_name='CODIGO', null=True)
    estado = CharField(column_name='ESTADO', null=True)
    id = AutoField(column_name='ID')
    zona = IntegerField(column_name='ZONA', null=True)

    class Meta:
        table_name = 'estado_codigo'

class ConfiguracionLeads(BaseModel):
    activa = IntegerField(column_name='ACTIVA', constraints=[SQL("DEFAULT 0")])
    descripcion = CharField(column_name='DESCRIPCION', null=True)
    id = AutoField(column_name='ID')
    estadosCodigos = ManyToManyField(model=EstadoCodigo, backref='Configuraciones')

    class Meta:
        table_name = 'configuracion_leads'

# ConfiguracionLeadsEstados = ConfiguracionLeads.estadosCodigos.get_through_model()

class ConfiguracionLeadsEstados(BaseModel):
    id_configuracion_leads = ForeignKeyField(column_name='ID_CONFIGURACION_LEADS', field='id', model=ConfiguracionLeads)
    id_estado_codigo = ForeignKeyField(column_name='ID_ESTADO_CODIGO', field='id', model=EstadoCodigo)

    class Meta:
        table_name = 'configuracion_leads_estados'
        primary_key = False

class Descartados(BaseModel):
    fecha = DateTimeField(column_name='FECHA', null=True)
    nombre = CharField(column_name='NOMBRE')
    servidor = IntegerField(column_name='SERVIDOR', null=True)

    class Meta:
        table_name = 'descartados'
        primary_key = False

class Diccionario(BaseModel):
    id = AutoField(column_name='ID')
    idioma = CharField(column_name='IDIOMA', null=True)
    palabra = CharField(column_name='PALABRA', null=True)

    class Meta:
        table_name = 'diccionario'

class Email(BaseModel):
    asignado = IntegerField(column_name='ASIGNADO', constraints=[SQL("DEFAULT 0")])
    email = CharField(column_name='EMAIL')
    id = AutoField(column_name='ID')

    class Meta:
        table_name = 'email'

class Pantalla(BaseModel):
    descripcion = CharField(column_name='DESCRIPCION', null=True)
    id = AutoField(column_name='ID')
    nombre = CharField(column_name='NOMBRE', null=True)
    opcion_padre = CharField(column_name='OPCION_PADRE', null=True)
    ruta = CharField(column_name='RUTA', null=True)

    class Meta:
        table_name = 'pantalla'

class ResultadoAnalisis(BaseModel):
    audio = CharField(column_name='AUDIO', null=True)
    estado = CharField(column_name='ESTADO', null=True)
    fecha = DateTimeField(column_name='FECHA', null=True)
    idioma = CharField(column_name='IDIOMA', null=True)
    servidor = IntegerField(column_name='SERVIDOR', null=True)
    telefono = CharField(column_name='TELEFONO')
    texto = CharField(column_name='TEXTO', null=True)

    class Meta:
        table_name = 'resultado_analisis'
        primary_key = False

class Rol(BaseModel):
    descripcion = CharField(column_name='DESCRIPCION', null=True)
    id = AutoField(column_name='ID')
    nombre = CharField(column_name='NOMBRE', null=True)

    class Meta:
        table_name = 'rol'

class RolPantalla(BaseModel):
    id = AutoField(column_name='ID')
    id_pantalla = ForeignKeyField(column_name='ID_PANTALLA', field='id', model=Pantalla, null=True)
    id_rol = ForeignKeyField(column_name='ID_ROL', field='id', model=Rol)

    class Meta:
        table_name = 'rol_pantalla'

class Telefonos(BaseModel):
    enviado = IntegerField(column_name='ENVIADO', null=True)
    fecha = DateTimeField(column_name='FECHA', null=True)
    id = BigAutoField(column_name='ID')
    id_estado_codigo = ForeignKeyField(column_name='ID_ESTADO_CODIGO', field='id', model=EstadoCodigo, null=True)
    numero_base = CharField(column_name='NUMERO_BASE', null=True)
    telefono = CharField(column_name='TELEFONO', null=True)

    class Meta:
        table_name = 'telefonos'

class TextoDescartable(BaseModel):
    id = AutoField(column_name='ID')
    texto = CharField(column_name='TEXTO', null=True)

    class Meta:
        table_name = 'texto_descartable'

class TipoIdentificacion(BaseModel):
    codigo = CharField(column_name='CODIGO', null=True)
    descripcion = CharField(column_name='DESCRIPCION', null=True)
    id = AutoField(column_name='ID')

    class Meta:
        table_name = 'tipo_identificacion'

class Usuario(BaseModel):
    activo = IntegerField(column_name='ACTIVO', constraints=[SQL("DEFAULT 0")])
    celular = CharField(column_name='CELULAR', null=True)
    contrasena = CharField(column_name='CONTRASENA')
    direccion = CharField(column_name='DIRECCION')
    fecha_activacion = DateTimeField(column_name='FECHA_ACTIVACION', null=True)
    fecha_registro = DateTimeField(column_name='FECHA_REGISTRO')
    fecha_ultima_modificacion = DateTimeField(column_name='FECHA_ULTIMA_MODIFICACION', null=True)
    fecha_ultimo_ingreso = DateTimeField(column_name='FECHA_ULTIMO_INGRESO', null=True)
    id = AutoField(column_name='ID')
    id_ciudad = ForeignKeyField(column_name='ID_CIUDAD', constraints=[SQL("DEFAULT 107")], field='id', model=Ciudad)
    id_email = ForeignKeyField(column_name='ID_EMAIL', field='id', model=Email)
    id_rol = ForeignKeyField(column_name='ID_ROL', field='id', model=Rol, null=True)
    id_tipo_identificacion = ForeignKeyField(column_name='ID_TIPO_IDENTIFICACION', field='id', model=TipoIdentificacion)
    numero_identificacion = CharField(column_name='NUMERO_IDENTIFICACION')
    primer_apellido = CharField(column_name='PRIMER_APELLIDO')
    primer_nombre = CharField(column_name='PRIMER_NOMBRE')
    segundo_apellido = CharField(column_name='SEGUNDO_APELLIDO', null=True)
    segundo_nombre = CharField(column_name='SEGUNDO_NOMBRE', null=True)
    telefono = CharField(column_name='TELEFONO', null=True)
    usuario = CharField(column_name='USUARIO')

    class Meta:
        table_name = 'usuario'

