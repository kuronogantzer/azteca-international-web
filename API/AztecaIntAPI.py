from flask import Flask, jsonify, request, redirect, url_for
from flask_cors import CORS, cross_origin
import eventlet.wsgi
from Negocio import * 
import logging
import socketio
from logging.handlers import RotatingFileHandler

JWT_SECRET = 'secret'
JWT_ALGORITHM = 'HS256'
JWT_EXP_DELTA_SECONDS = 300

sio = socketio.Server()
app = Flask(__name__)

cors = CORS(app)
app.config['CORS_HEADERS'] = 'Content-Type'

negocio = Negocio()

@app.route('/EstadosCodigos', methods=['GET'])
@cross_origin()
def ConsultarEstadosCodigos():
    resultado = negocio.ConsultarEstadosCodigos()
    return jsonify(resultado), 200

@app.route('/Usuario', methods=['GET'])
@cross_origin()
def ConsultarUsuarios():
    resultado = negocio.ConsultarUsuarios()
    return jsonify(resultado), 200

@app.route('/Login', methods=['POST'])
def Login():
    data = request.get_json()
    requeridos = ['usuario', 'contrasena']
    if not all(k in data for k in requeridos):
        return 'Datos incompletos', 400
    resultado = negocio.Login(data['usuario'], data['contrasena'], request.remote_addr)
    if resultado is None:
        return 'Usuario o crontraseña erróneos', 400
    return jsonify(resultado), 200

@app.route('/Token', methods=['POST'])
@cross_origin()
def NuevoToken():
    data = request.get_json()
    requeridos = ['idUsuario']
    if not all(k in data for k in requeridos):
        return 'Datos incompletos', 400
    resultado = negocio.GenerarToken(data['idUsuario'], request.remote_addr)
    return jsonify(resultado), 200

@app.route('/Token/<int:idUsuario>', methods=['DELETE'])
@cross_origin()
def EliminarToken(idUsuario):
    negocio.EliminarToken(idUsuario)
    return jsonify(True), 200

@app.route('/Usuario', methods=['POST'])
@cross_origin()
def CrearUsuario():
    data = request.get_json()
    requeridos = ['contrasena', 'primer_nombre', 'primer_apellido', 'direccion', 'email', 'ciudad',
                  'fecha_registro', 'numero_identificacion', 'tipo_identificacion', 'usuario', 'rol']
    if not all(k in data for k in requeridos):
        return 'Datos incompletos', 400
    resultado = negocio.NuevoUsuario(data)
    if resultado is None or resultado is False:
        return 'Ha ocurrido un error al crear el usuario', 400
    return jsonify(resultado), 200

@app.route('/Usuario', methods=['PUT'])
@cross_origin()
def ActualizarUsuario():
    data = request.get_json()
    requeridos = ['id', 'primer_nombre', 'primer_apellido', 'direccion', 'email', 'ciudad',
                  'numero_identificacion', 'tipo_identificacion', 'usuario', 'rol']
    if not all(k in data for k in requeridos):
        return 'Datos incompletos', 400
    resultado = negocio.ActualizarUsuario(data)
    if not resultado:
        return 'Ha ocurrido un error al crear el usuario', 400
    return jsonify(resultado), 200

@app.route('/Usuario/<int:id>', methods=['DELETE'])
@cross_origin()
def InactivarUsuarios(id):
    resultado = negocio.InactivarUsuario(id)
    return jsonify(resultado), 200

@app.route('/Permisos', methods=['POST'])
@cross_origin()
def VerificarPermisos():
    datos = request.get_json()
    requeridos = ['idUsuario', 'ruta']
    if not all(k in datos for k in requeridos):
        return 'Datos incompletos', 400
    resultado = negocio.VerificarPermisos(datos['idUsuario'], datos['ruta'])
    return jsonify(resultado), 200

@app.route('/TipoDocumento', methods=['GET'])
@cross_origin()
def ConsultarTiposDocumento():
    resultado = negocio.ConsultarTiposDocumento()
    return jsonify(resultado), 200

@app.route('/Ciudad', methods=['GET'])
@cross_origin()
def ConsultarCiudades():
    resultado = negocio.ConsultarCiudades()
    return jsonify(resultado), 200

@app.route('/Usuario/<int:id>', methods=['GET'])
@cross_origin()
def ConsultarUsuario(id):
    resultado = negocio.ConsultarUsuarioPorId(id)
    if resultado is None:
        return 'No se encontró el usuario.', 400
    return jsonify(resultado), 200

@app.route('/Rol', methods=['GET'])
@cross_origin()
def ConsultarRoles():
    resultados = negocio.ConsultarRoles()
    if resultados is None:
        return 'No hay roles creados en el sistema', 400
    return jsonify(resultados), 200

@app.route('/Rol/<int:id>', methods=['GET'])
@cross_origin()
def ConsultarRolPorId(id):
    resultado = negocio.ConsultarRolPorId(id)
    if resultado is None:
        return 'No se encontro el rol', 400
    return jsonify(resultado), 200

@app.route('/Rol', methods=['POST'])
@cross_origin()
def CrearRol():
    data = request.get_json()
    requeridos = ['nombre', 'descripcion', 'rolPantallas']
    if not all(k in data for k in requeridos):
        return 'Datos incompletos', 400
    resultado = negocio.CrearRol(data)
    return jsonify(resultado), 200

@app.route('/Rol', methods=['PUT'])
@cross_origin()
def ActualizarRol():
    data = request.get_json()
    requeridos = ['id', 'nombre', 'descripcion', 'rolPantallas']
    if not all(k in data for k in requeridos):
        return 'Datos incompletos', 400
    resultado = negocio.ActualizarRol(data)
    return jsonify(resultado), 200

@app.route('/Rol/<int:id>', methods=['DELETE'])
@cross_origin()
def EliminarRol(id):
    resultado = negocio.EliminarRol(id)
    return jsonify(resultado), 200

@app.route('/Pantalla', methods=['GET'])
@cross_origin()
def ConsultarPantallas():
    resultado = negocio.ConsultarPantallas()
    return jsonify(resultado), 200

@app.route('/GenerarLeads', methods=['POST'])
@cross_origin()
def GenerarLeads():
    data = request.get_json()
    resultado = negocio.GenerarLeads(data)
    return jsonify(resultado), 200

@app.route('/Email', methods=['GET'])
@cross_origin()
def ConsultarEmails():
    resultados = negocio.ConsultarEmails()
    return jsonify(resultados), 200

@app.route('/Email/<int:estado>', methods=['GET'])
@cross_origin()
def ConsultarEmailsPorEstado(estado:int):
    if estado is 1 or estado is 0:
        resultados = negocio.ConsultarEmailPorEstado(estado)
        return jsonify(resultados), 200
    else:
        return 'Estado no existente', 400

@app.route('/Email', methods=['POST'])
@cross_origin()
def RegistrarEmail():
    data = request.get_json()
    requeridos = ['email']
    if not all(k in data for k in requeridos):
        return 'Datos incompletos', 400
    resultado = negocio.RegistrarEmail(data)
    return jsonify(resultado), 200

@app.route('/Email', methods=['PUT'])
@cross_origin()
def ActualizarEmail():
    data = request.get_json()
    requeridos = ['id', 'email']
    if not all(k in data for k in requeridos):
        return 'Datos incompletos', 400
    resultado = negocio.ActualizarEmail(data)
    return jsonify(resultado), 200

@app.route('/Email/<int:idEmail>', methods=['DELETE'])
@cross_origin()
def EliminarEmail(idEmail:int):
    resultado = negocio.EliminarEmail(idEmail)
    return jsonify(resultado), 200

@app.route('/ConfiguracionLeads', methods=['GET'])
@cross_origin()
def ConsultarConfiguracionesLeads():
    resultado = negocio.ConsultarConfiguracionesLeads()
    return jsonify(resultado), 200

@app.route('/ConfiguracionLeads/<int:idConfiguracionLeads>', methods=['GET'])
@cross_origin()
def ConsultarConfiguracionLeads(idConfiguracionLeads):
    resultado = negocio.ConsultarConfiguracionLeads(idConfiguracionLeads)
    return jsonify(resultado), 200

@app.route('/ConfiguracionLeads', methods=['POST'])
@cross_origin()
def CrearConfiguraciónLeads():
    data = request.get_json()
    requeridos = ['descripcion']
    if not all(k in data for k in requeridos):
        return 'Datos incompletos', 400
    resultado = negocio.CrearConfiguracionLeads(data)
    return jsonify(resultado), 200

@app.route('/ConfiguracionLeads', methods=['PUT'])
@cross_origin()
def ActualizarConfiguracionLeads():
    data = request.get_json()
    requeridos = ['id', 'descripcion']
    if not all(k in data for k in requeridos):
        return 'Datos incompletos', 400
    resultado = negocio.AcualizarConfiguracionLeads(data)
    return jsonify(resultado), 200

@app.route('/ConfiguracionLeads/<int:idConfiguracionLeads>', methods=['DELETE'])
@cross_origin()
def EliminarConfiguracionLeads(idConfiguracionLeads):
    resultado = negocio.EliminarConfiguracionLeads(idConfiguracionLeads)
    return jsonify(resultado), 200

@app.route('/CambiarEstadoConfiguracionLeads/<int:idConfiguracion>', methods=['GET'])
@cross_origin()
def CambiarEstadoConfiguracionLeads(idConfiguracion):
    resultado = negocio.CambiarEstadoConfiguracionLeads(idConfiguracion)
    # data = request.get_json()
    # requeridos = ['idConfiguracionLeads', 'estado']
    # if not all(k in data for k in requeridos):
    #     return 'Datos incompletos', 400
    # resultado = negocio.CambiarEstadoConfiguracionLeads(data['idConfiguracionLeads'], data['estado'])
    return jsonify(resultado), 200

@sio.on('connect')
def connect(sid, environ):
    print("Se conectó: ", sid)

@sio.on('disconnect')
def disconnect(sid):
    print("Se desconectó: ", sid)

exporting_threads = {}

@sio.on('GenerarLeads')
def GenerarLeads(sid, data):
    estadosCodigosSeleccionados = [dict_to_model(EstadoCodigo, estadoCodigo, True)
                                   for estadoCodigo in data]
    nuevosTelefonos = []
    for codigoSeleccionado in estadosCodigosSeleccionados:
        global exporting_threads
        thread_id = random.randint(0, 10000)
        exporting_threads[thread_id] = Multiproceso(codigoSeleccionado)
        exporting_threads[thread_id].start()
        # out1 = Negocio.GenerarLeadsCodigo(codigoSeleccionado)
        # nuevosTelefonos = nuevosTelefonos + out1
        sio.emit('Respuesta', exporting_threads[thread_id].progress, room=sid)
    # Telefonos.insert_many(nuevosTelefonos).execute()
    # resultado = negocio.GenerarLeads(data, sio, sid)
    # print("Mensaje recibido: ", data)
    # sio.emit('Respuesta', resultado, room=sid)

# async def auth_middleware(app, handler):
#     async def middleware(request):
#         request.user = None
#         jwt_token = request.headers.get('authoriation', None)
#         if jwt_token:
#             try:
#                 payload = jwt.decode(jwt_token, JWT_SECRET, algorithms=[JWT_ALGORITHM])
#             except (jwt.DecodeError, jwt.ExpiredSignatureError):
#                 return jsonify({'message': 'Token is invalid'}), 400
#             request.user = payload['user_id']
#         return await handler(request)
#     return middleware

@app.before_request
def ValidarToken():
    jwt_token = request.headers.get('Authorization', None)
    if jwt_token and request.endpoint != 'Login':
        try:
            payload = negocio.ValidarToken(jwt_token)
        except Exception as error:
            print(error)
            return jsonify({'message': 'El Token no es válido'}), 400
    # elif jwt_token is None:
    #     return jsonify({'message': 'Acceso no permitido sin token'}), 400

if __name__ == '__main__':
    from argparse import ArgumentParser
    parser = ArgumentParser()
    parser.add_argument('-p', '--port', default=5000, type=int, help='port to listen on')
    args = parser.parse_args()
    port = args.port

    # formatter = logging.Formatter(
    #     "[%(asctime)s] %(levelname)s - %(message)s")
    # handler = RotatingFileHandler('error.log', maxBytes=10000000, backupCount=5)
    # handler.setLevel(logging.DEBUG)
    # handler.setFormatter(formatter)
    # log = logging.getLogger('werkzeug')
    # log.setLevel(logging.DEBUG)
    # log.addHandler(handler)
    # app.logger.addHandler(handler)

    # import logging
    # logging.basicConfig(filename='error.log', level=logging.DEBUG)

    # app = socketio.Middleware(sio, app)
    # eventlet.wsgi.server(eventlet.listen(('',port)), app)
    app.run(host='0.0.0.0', port=port)