import { Directive, ElementRef, HostListener } from '@angular/core';

@Directive({
    selector: '[solotexto]'
})
export class SolotextoDirective {

    constructor(private element:ElementRef) {
    }

    @HostListener('keydown', ['$event'])
    TeclaPresionada(event) {
        let regexpr = new RegExp('[a-zA-ZÀ-ÿ\u00f1\u00d1]*(\s*[a-zA-ZÀ-ÿ\u00f1\u00d1]*)*[a-zA-ZÀ-ÿ\u00f1\u00d1]+');
        if (!regexpr.test(event.key) && event.key.length === 1)
            event.preventDefault();
    }
}
