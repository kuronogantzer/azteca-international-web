import { Directive, ElementRef, HostListener } from '@angular/core';

@Directive({
    selector: '[solonumero]'
})
export class SolonumeroDirective {

    constructor(private element:ElementRef) {
    }

    @HostListener('keydown', ['$event'])
    TeclaPresionada(event) {
        let regexpr = new RegExp('[0-9]');
        if (!regexpr.test(event.key) && event.key.length === 1)
            event.preventDefault();
    }

}
