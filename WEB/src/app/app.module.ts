import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ToasterModule, ToasterService } from 'angular2-toaster';
import { APP_ROUTING } from './app.routes';
import { SocketIoModule, SocketIoConfig } from 'ngx-socket-io';
import { environment } from '../environments/environment';

/* MULRISELECT DROPDOWN */
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown';

/* ANGULAR MATERIAL */
import { MatInputModule } from '@angular/material/input';
import { MatMenuModule } from '@angular/material/menu';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatStepperModule, MatStepperIntl } from '@angular/material/stepper';
import { MatNativeDateModule } from '@angular/material';
import { MatSelectModule } from '@angular/material/select';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatTableModule } from '@angular/material/table';
import { MatSortModule } from '@angular/material/sort';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';

/* INTERCEPTORS */
import { ErrorInterceptor } from './interceptors/error.interceptor';
import { JwtInterceptor } from './interceptors/jwtInterceptor';

/* SERVICIOS */
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { SeguridadService } from './servicios/seguridad.service';
import { LeadsService } from './servicios/leads.service';
import { CargandoService } from './servicios/cargando.service';

// import { WebsocketService } from './servicios/websocket.service';

import { AppComponent } from './app.component';
import { MenuComponent } from './componentes/compartidos/menu/menu.component';
import { InicioComponent } from './componentes/inicio/inicio.component';
import { LoginComponent } from './componentes/login/login.component';
import { UsuariosComponent } from './componentes/configuracion/usuarios/usuarios.component';
import { SolotextoDirective } from './directivas/solotexto.directive';
import { SolonumeroDirective } from './directivas/solonumero.directive';
import { UsuarioDetalleComponent } from './componentes/configuracion/usuarios/usuario-detalle/usuario-detalle.component';
import { RolesComponent } from './componentes/configuracion/roles/roles.component';
import { RolDetalleComponent } from './componentes/configuracion/roles/rol-detalle/rol-detalle.component';
import { LeadsComponent } from './componentes/configuracion-leads/leads/leads.component';
import { AudiosComponent } from './componentes/audios/audios.component';
import { EmailsComponent } from './componentes/emails/emails.component';
import { CargandoComponent } from './componentes/compartidos/cargando/cargando.component';
import { ConfiguracionLeadsComponent } from './componentes/configuracion-leads/configuracion-leads.component';
const config: SocketIoConfig = { url: 'http://localhost:5000', options: {} };

@NgModule({
    declarations: [
        AppComponent,
        MenuComponent,
        InicioComponent,
        LoginComponent,
        UsuariosComponent,
        SolotextoDirective,
        SolonumeroDirective,
        UsuarioDetalleComponent,
        RolesComponent,
        RolDetalleComponent,
        LeadsComponent,
        AudiosComponent,
        EmailsComponent,
        CargandoComponent,
        ConfiguracionLeadsComponent
    ],
    imports: [
        BrowserModule,
        FormsModule,
        ToasterModule.forRoot(),
        AngularMultiSelectModule,
        ReactiveFormsModule,
        HttpClientModule,
        BrowserAnimationsModule,
        MatInputModule,
        MatMenuModule,
        MatToolbarModule,
        MatStepperModule,
        MatNativeDateModule,
        MatSelectModule,
        MatAutocompleteModule,
        MatTableModule,
        MatSortModule,
        MatCheckboxModule,
        MatProgressSpinnerModule,
        MatSlideToggleModule,
        SocketIoModule.forRoot(config),
        APP_ROUTING
    ],
    providers: [
        SeguridadService,
        LeadsService,
        ToasterService,
        // WebsocketService,
        MatStepperIntl,
        CargandoService,
        { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
        { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true }
    ],
    bootstrap: [AppComponent]
})
export class AppModule { }
