import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './componentes/login/login.component';
import { InicioComponent } from './componentes/inicio/inicio.component';
import { UsuariosComponent } from './componentes/configuracion/usuarios/usuarios.component';
import { UsuarioDetalleComponent } from './componentes/configuracion/usuarios/usuario-detalle/usuario-detalle.component';
import { RolesComponent } from './componentes/configuracion/roles/roles.component';
import { RolDetalleComponent } from './componentes/configuracion/roles/rol-detalle/rol-detalle.component';
import { LeadsComponent } from './componentes/configuracion-leads/leads/leads.component';
import { AudiosComponent } from './componentes/audios/audios.component';
import { EmailsComponent } from './componentes/emails/emails.component';
import { ConfiguracionLeadsComponent } from './componentes/configuracion-leads/configuracion-leads.component';
import { AuthGuard } from './servicios/auth.guard';

const APP_ROUTES: Routes = [
  { path: 'inicio', component: InicioComponent, canActivate: [AuthGuard] },
  { path: 'login', component: LoginComponent },
  { path: 'usuarios', component: UsuariosComponent, canActivate: [AuthGuard] },
  { path: 'usuario', component: UsuarioDetalleComponent, canActivate: [AuthGuard] },
  { path: 'usuario/:id', component: UsuarioDetalleComponent, canActivate: [AuthGuard] },
  { path: 'roles', component: RolesComponent, canActivate: [AuthGuard] },
  { path: 'rol', component: RolDetalleComponent, canActivate: [AuthGuard] },
  { path: 'rol/:id', component: RolDetalleComponent, canActivate: [AuthGuard] },
  { path: 'audios', component: AudiosComponent, canActivate: [AuthGuard] },
  { path: 'emails', component: EmailsComponent, canActivate: [AuthGuard] },
  { path: 'configuracion-leads', component: ConfiguracionLeadsComponent, canActivate: [AuthGuard] },
  { path: 'leads', component: LeadsComponent, canActivate: [AuthGuard] },
  { path: 'leads/:id', component: LeadsComponent, canActivate: [AuthGuard] },
  { path: '**', pathMatch: 'full', redirectTo: 'inicio' }
];

export const APP_ROUTING = RouterModule.forRoot(APP_ROUTES);
