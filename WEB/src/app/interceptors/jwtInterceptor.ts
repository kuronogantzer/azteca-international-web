import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { Observable } from 'rxjs';
import { SeguridadService } from '../servicios/seguridad.service';

@Injectable()
export class JwtInterceptor implements HttpInterceptor {

    constructor(private _seguridadService: SeguridadService){}

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        let currentUser = JSON.parse(localStorage.getItem('currentUser'));
        if (currentUser && request.url != 'http://localhost:5000/Token' && request.url != 'http://localhost:5000/Login') {
            this._seguridadService.RefrescarToken(currentUser.usuario.id)
                .subscribe( (data:any) => {
                    if (currentUser && currentUser.token) {
                        currentUser.token = data;
                        localStorage.setItem('currentUser', JSON.stringify(currentUser));
                        request = request.clone({
                            setHeaders: {
                                Authorization: data
                            }
                        });
                    }
                });
        }
        else if (currentUser && currentUser.token) {
            request = request.clone({
                setHeaders: {
                    Authorization: currentUser.token
                }
            });
        }
        return next.handle(request);
    }
}
