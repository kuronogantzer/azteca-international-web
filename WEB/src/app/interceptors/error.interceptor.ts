import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { ToasterService } from 'angular2-toaster';
import { SeguridadService } from '../servicios/seguridad.service';

@Injectable()
export class ErrorInterceptor implements HttpInterceptor {
    constructor(private authenticationService: SeguridadService,
                private _toasterService: ToasterService,) {}

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        return next.handle(request).pipe(catchError(err => {
            if (err.status === 401 || err.status === 400) {
                // auto logout if 401 response returned from api
                this.authenticationService.Logout();
                this._toasterService.pop({
                    type: 'error',
                    title: 'La sesión se ha vencido'
                });
                location.reload(true);
            }

            const error = err.error.message || err.statusText;
            return throwError(error);
        }))
    }
}
