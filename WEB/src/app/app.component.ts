import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { ToasterConfig } from 'angular2-toaster';
import { Pantalla } from './modelos/modelos';
import { SeguridadService } from './servicios//seguridad.service';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css']
})
export class AppComponent {
    logged: boolean;
    title = 'WEB';
    public pantallas: Pantalla[] = [];

    constructor(private _router: Router,
                private _seguridadService: SeguridadService) {
                    this.logged = this._seguridadService.ValidarToken();
                    this.pantallas = this._seguridadService.ActualizarPermisos();
                }

    private toastConfig = new ToasterConfig({
        positionClass: 'toast-bottom-center',
        showCloseButton: false,
        timeout: 5000});

    login(data: any) {
        this.logged = this._seguridadService.ValidarToken();
        this.pantallas = data.pantallas;
        this._router.navigate(['inicio']);
    }
}
