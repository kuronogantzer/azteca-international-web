import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormControl, Validators, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { CargandoService } from '../../servicios/cargando.service';
import { SeguridadService } from '../../servicios/seguridad.service';
import { ToasterService } from 'angular2-toaster';
import { Usuario, Pantalla} from '../../modelos/modelos';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

    formulario: FormGroup;
    @Output() logged: EventEmitter<any>;

    constructor(private _seguridadService: SeguridadService,
                private _toasterService: ToasterService,
                private _cargandoService: CargandoService,
                private _router: Router) {
        this.logged = new EventEmitter();
    }

    ngOnInit() {
        if (this._seguridadService.ValidarToken()) {
            this._router.navigate(['inicio'])
        }
        this.formulario = new FormGroup({
            'usuario': new FormControl('', [Validators.required]),
            'contrasena': new FormControl('', [Validators.required])
        });
    }

    Login() {
        if (this.formulario.valid) {
            this._cargandoService.mostrar();
            this._seguridadService.Login(this.formulario.value)
            .subscribe( (data: any) => {
                if (typeof data !== 'string') {
                    const usuario: Usuario = data.usuario;
                    const pantallas: Pantalla[] = data.rolesPantallas.map( (rolPantalla) => rolPantalla.pantalla );
                    // this._seguridadService.token = data.token;
                    this._toasterService.pop({
                        type: 'success',
                        title: 'Login Exitoso'
                    });
                    this._seguridadService.usuario = usuario;
                    this._cargandoService.ocultar();
                    this.logged.emit({logged: true, pantallas: pantallas});
                } else {
                    this._toasterService.pop({
                        type: 'error',
                        title: 'Usuario o contraseña incorrectos'
                    });
                    this._cargandoService.ocultar();
                }
            }, (error) => {
                this._toasterService.pop({
                    type: 'error',
                    title: 'Servidor no disponible'
                });
                this._cargandoService.ocultar();
            });
        }
    }
}
