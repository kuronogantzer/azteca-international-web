import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { SeguridadService } from '../../servicios/seguridad.service';
import { CargandoService } from '../../servicios/cargando.service';
import { LeadsService } from '../../servicios/leads.service';
import { ConfiguracionLeads } from '../../modelos/modelos';
import { MatSort, MatTableDataSource } from '@angular/material';
import { ToasterService } from 'angular2-toaster';

@Component({
    selector: 'app-configuracion-leads',
    templateUrl: './configuracion-leads.component.html',
    styles: []
})
export class ConfiguracionLeadsComponent implements OnInit {

    configuraciones: ConfiguracionLeads[] = [];
    configuracionesFiltradas: ConfiguracionLeads[] = [];
    dataSource = new MatTableDataSource(this.configuracionesFiltradas);
    filtro: string;
    displayedColumns: string[] = ['activa', 'descripcion', 'edit-delete'];
    @ViewChild(MatSort) sort: MatSort;

    constructor(private _seguridadService:SeguridadService,
                private _leadsService:LeadsService,
                private _toasterService:ToasterService,
                private _cargandoService: CargandoService,
                private _router: Router) { }

    ngOnInit() {
        this._cargandoService.mostrar();
        this._seguridadService.VerificarPermisos(this._seguridadService.usuario.id, '/configuracion-leads')
            .subscribe( data => {
                if (data) {
                    this.ConsultarConfiguracionesLeads();
                }
                else {
                    this._toasterService.pop({
                        type: 'error',
                        title: 'Usuario no autorizado para acceder'
                    });
                    this._cargandoService.ocultar();
                    this._router.navigate(['inicio']);
                }
            });
    }

    ConsultarConfiguracionesLeads() {
        this._leadsService.ConsultarConfiguracionesLeads()
            .subscribe( (data:ConfiguracionLeads[]) => {
                this.configuraciones = data;
                this.configuracionesFiltradas = this.configuraciones;
                this.dataSource = new MatTableDataSource(this.configuracionesFiltradas);
                this.dataSource.sort = this.sort;
                this._cargandoService.ocultar();
            });
    }

    filtrar(filtro:string) {
        this.dataSource.filter = filtro.trim().toLowerCase();
    }

    EliminarConfiguracion(id) {
        this._cargandoService.mostrar();
        this._leadsService.EliminarConfiguracionLeads(id)
            .subscribe( (data:any) => {
                if (data === true) {
                    this._toasterService.pop({
                        type: 'success',
                        title: 'Configuración eliminada'
                    });
                    this.ConsultarConfiguracionesLeads();
                }
                else {
                    this._cargandoService.ocultar();
                }
            });
    }

    CambiarEstadoConfiguracionLeads(id, estado) {
        this._cargandoService.mostrar();
        this._leadsService.CambiarEstadoConfiguracionLeads(id, estado)
            .subscribe( (data) => {
                if (data) {
                    this._cargandoService.ocultar();
                }
                else {
                    this._toasterService.pop({
                        type: 'error',
                        title: 'Error al activa o desactivar la configuración'
                    });
                    this._cargandoService.ocultar();
                }
            }, (err) => {
                this._toasterService.pop({
                    type: 'error',
                    title: err.toString()
                });
                this._cargandoService.ocultar();
            });
    }

}
