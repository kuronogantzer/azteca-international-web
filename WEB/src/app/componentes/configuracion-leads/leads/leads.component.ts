import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { LeadsService } from '../../../servicios/leads.service';
import { SeguridadService } from '../../../servicios/seguridad.service';
import { EstadoCodigo, ConfiguracionLeads } from '../../../modelos/modelos';
import { ToasterService } from 'angular2-toaster';
import { CargandoService } from '../../../servicios/cargando.service';
// import { WebsocketService } from '../../servicios/websocket.service';
// import { Message, Event, Action, User} from '../../modelos/WSModels';

@Component({
    selector: 'app-leads',
    templateUrl: './leads.component.html',
    styles: []
})
export class LeadsComponent implements OnInit {

    configuracionLeads: ConfiguracionLeads;
    estadosCodigos: EstadoCodigo[] = [];
    descripcion: string;
    generando = false;
    editando: boolean = false;
    estadosZona1 = [];
    estadosZona2 = [];
    estadosZona3 = [];
    estadosZona4 = [];
    estadosSeleccionados1 = [];
    estadosSeleccionados2 = [];
    estadosSeleccionados3 = [];
    estadosSeleccionados4 = [];
    configuracion = {
        text:"Seleccione algún estado",
        selectAllText:'Seleccionar todos',
        unSelectAllText:'Limpiar',
        filterSelectAllText:'Seleccionar filtrados',
        filterUnSelectAllText: 'Limpiar filtrados',
        searchPlaceholderText: 'Buscar',
        singleSelection: false,
        enableSearchFilter: true
    };
    // ioConnection:any;
    // messages: Message[] = [];
    // messageContent: string;

    constructor(private _leadsService:LeadsService,
                private _toasterService:ToasterService,
                private _seguridadService: SeguridadService,
                private _cargandoService: CargandoService,
                private _activatedRoute: ActivatedRoute,
                private _router: Router) { }

    ngOnInit() {
        this._seguridadService.VerificarPermisos(this._seguridadService.usuario.id, '/leads')
            .subscribe( data => {
                if (data) {
                    this.Iniciar();
                }
                else {
                    this._toasterService.pop({
                        type: 'error',
                        title: 'Usuario no autorizado para acceder'
                    });
                    this._router.navigate(['inicio']);
                }
            });

    }

    Iniciar() {
        this._activatedRoute.params.subscribe( (params) => {
            this._leadsService.ConsultarEstadosCodigos()
                .subscribe( (data:any) => {
                    this.estadosCodigos = data;
                    let i = 0;
                    this.estadosCodigos.forEach( (estadoCodigo) => {
                        if (estadoCodigo.zona === 1) {
                            if (this.estadosZona1.filter( estado => { return estado.itemName == estadoCodigo.estado }).length == 0) {
                                this.estadosZona1.push({
                                    id: estadoCodigo.id,
                                    itemName: estadoCodigo.estado });
                                }
                            }
                        if (estadoCodigo.zona === 2) {
                            if (this.estadosZona2.filter( estado => { return estado.itemName == estadoCodigo.estado }).length == 0) {
                                this.estadosZona2.push({
                                    id: estadoCodigo.id,
                                    itemName: estadoCodigo.estado });
                            }
                        }
                        if (estadoCodigo.zona === 3) {
                            if (this.estadosZona3.filter( estado => { return estado.itemName == estadoCodigo.estado }).length == 0) {
                                this.estadosZona3.push({
                                    id: estadoCodigo.id,
                                    itemName: estadoCodigo.estado });
                                }
                        }
                        if (estadoCodigo.zona === 4) {
                            if (this.estadosZona4.filter( estado => { return estado.itemName == estadoCodigo.estado }).length == 0) {
                                this.estadosZona4.push({
                                    id: estadoCodigo.id,
                                    itemName: estadoCodigo.estado });
                                }
                        }
                        i++;
                    });
                });
            if (params['id']) {
                this.editando = true;
                this._leadsService.ConsultarConfiguracionLeads(params['id'])
                    .subscribe( (data:any) => {
                        this.configuracionLeads = data;
                        this.configuracionLeads.estadosCodigos.forEach((estadoCodigo:EstadoCodigo) => {
                            if(estadoCodigo.zona === 1 && this.estadosSeleccionados1.filter( (estadoSeleccionado:any) => { return estadoSeleccionado.itemName === estadoCodigo.estado; }).length === 0) {
                                this.estadosSeleccionados1.push({
                                    id: estadoCodigo.id,
                                    itemName: estadoCodigo.estado
                                });
                            }
                            else if(estadoCodigo.zona === 2 && this.estadosSeleccionados2.filter( (estadoSeleccionado:any) => { return estadoSeleccionado.itemName === estadoCodigo.estado; }).length === 0) {
                                this.estadosSeleccionados2.push({
                                    id: estadoCodigo.id,
                                    itemName: estadoCodigo.estado
                                });
                            }
                            else if(estadoCodigo.zona === 3 && this.estadosSeleccionados3.filter( (estadoSeleccionado:any) => { return estadoSeleccionado.itemName === estadoCodigo.estado; }).length === 0) {
                                this.estadosSeleccionados3.push({
                                    id: estadoCodigo.id,
                                    itemName: estadoCodigo.estado
                                });
                            }
                            else if(estadoCodigo.zona === 4 && this.estadosSeleccionados4.filter( (estadoSeleccionado:any) => { return estadoSeleccionado.itemName === estadoCodigo.estado; }).length === 0) {
                                this.estadosSeleccionados4.push({
                                    id: estadoCodigo.id,
                                    itemName: estadoCodigo.estado
                                });
                            }
                        });
                        this.descripcion = this.configuracionLeads.descripcion;
                    }, (err) => {

                    });
            } else {
                this.editando = false;
                this._cargandoService.ocultar();
            }

        });
            // this._leadsService.Respuesta().subscribe((data) => {
            //     console.log(data);
            // });
    }

    CrearConfiguracion() {
        let codigos: EstadoCodigo[] = [];
        if(this.estadosSeleccionados1.length > 0 && this.descripcion) {
            this._cargandoService.mostrar();
            this.estadosCodigos.forEach( estadoCodigo => {
                if (this.estadosSeleccionados1.filter( estadoSeleccionado => { return estadoSeleccionado.itemName == estadoCodigo.estado; }).length > 0) {
                    codigos.push(estadoCodigo);
                }
            });
        }
        if(this.estadosSeleccionados2.length > 0) {
            this.estadosCodigos.forEach( estadoCodigo => {
                if (this.estadosSeleccionados2.filter( estadoSeleccionado => { return estadoSeleccionado.itemName == estadoCodigo.estado; }).length > 0) {
                    codigos.push(estadoCodigo);
                }
            });
        }
        if(this.estadosSeleccionados3.length > 0) {
            this.estadosCodigos.forEach( estadoCodigo => {
                if (this.estadosSeleccionados3.filter( estadoSeleccionado => { return estadoSeleccionado.itemName == estadoCodigo.estado; }).length > 0) {
                    codigos.push(estadoCodigo);
                }
            });
        }
        if(this.estadosSeleccionados4.length > 0) {
            this.estadosCodigos.forEach( estadoCodigo => {
                if (this.estadosSeleccionados4.filter( estadoSeleccionado => { return estadoSeleccionado.itemName == estadoCodigo.estado; }).length > 0) {
                    codigos.push(estadoCodigo);
                }
            });
        }
        this.configuracionLeads = this.editando ? this.configuracionLeads : new ConfiguracionLeads();
        this.configuracionLeads.descripcion = this.descripcion;
        this.configuracionLeads.activa = false;
        this.configuracionLeads.estadosCodigos = codigos;
        if (this.editando) {
            this._leadsService.ActualizarConfiguracionLeads(this.configuracionLeads)
                .subscribe( data => {
                    this._cargandoService.ocultar();
                    this._toasterService.pop({
                        type: 'success',
                        title: 'Configuración actualizada e INACTIVA'
                    });
                    this._router.navigate(['configuracion-leads']);
                })
        } else {
            this._leadsService.CrearConfiguracionLeads(this.configuracionLeads)
            .subscribe( data => {
                this._cargandoService.ocultar();
                this._toasterService.pop({
                    type: 'success',
                    title: 'Configuración creada con éxito'
                });
                this._router.navigate(['configuracion-leads']);
            });
        }
        //this._leadsService.GenerarLEads(codigos);
    }

    // private initIoConnection(): void {
    //     this.socketService.initSocket();
    //
    //     this.ioConnection = this.socketService.onMessage()
    //       .subscribe((message: Message) => {
    //         this.messages.push(message);
    //       });
    //
    //     this.socketService.onEvent(Event.CONNECT)
    //       .subscribe(() => {
    //         console.log('connected');
    //       });
    //
    //     this.socketService.onEvent(Event.DISCONNECT)
    //       .subscribe(() => {
    //         console.log('disconnected');
    //       });
    // }

}
