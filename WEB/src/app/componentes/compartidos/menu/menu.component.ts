import { Component, OnInit, Input } from '@angular/core';
import { Pantalla } from '../../../modelos/modelos';
import { SeguridadService } from '../../../servicios/seguridad.service';

@Component({
    selector: 'app-menu',
    templateUrl: './menu.component.html',
    styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {

    @Input() pantallas: Pantalla[] = [];
    menu = [];

    constructor(private _seguridadService: SeguridadService) { }

    ngOnInit() {
        this.pantallas.forEach( (pantalla) => {
            if (pantalla.opcion_padre) {
                let opcionPadre = this.menu.filter( (opcion) => opcion.padre === pantalla.opcion_padre)[0];
                if (opcionPadre && opcionPadre.hijos.filter( (hijo) => hijo.ruta === pantalla.ruta).length === 0) {
                    opcionPadre.hijos.push({ nombre: pantalla.nombre, ruta: pantalla.ruta });
                } else {
                    this.menu.push({ padre:pantalla.opcion_padre, hijos: [{ nombre: pantalla.nombre, ruta: pantalla.ruta }] });
                }
            } else if (this.menu.filter( (opcion) => opcion.ruta === pantalla.ruta).length === 0) {
              this.menu.push({ nombre: pantalla.nombre, ruta: pantalla.ruta });
            }
        });
    }

    CerrarSesion() {
        this._seguridadService.Logout()
            .subscribe( (data) => {
                location.reload();
            });
    }

}
