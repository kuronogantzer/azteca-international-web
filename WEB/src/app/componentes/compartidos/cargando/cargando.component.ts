import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';

import { CargandoService } from '../../../servicios/cargando.service';
import { EstadoCargando } from '../../../modelos/cargando';

@Component({
    selector: 'app-cargando',
    templateUrl: './cargando.component.html',
    styleUrls: ['./cargando.component.css']
})
export class CargandoComponent implements OnInit {

    mostrar = false;
    private subscription: Subscription;

    constructor(private _cargandoService: CargandoService) { }

    ngOnInit() {
        this.subscription = this._cargandoService.estadoCargando
            .subscribe((estado: EstadoCargando) => {
                this.mostrar = estado.mostrar;
            });
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
    }

}
