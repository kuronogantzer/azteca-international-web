import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { MatSort, MatTableDataSource } from '@angular/material';
import { SeguridadService } from '../../servicios/seguridad.service';
import { ToasterService } from 'angular2-toaster';
import { Email } from '../../modelos/modelos';
import { CargandoService } from '../../servicios/cargando.service';

@Component({
    selector: 'app-emails',
    templateUrl: './emails.component.html',
    styles: []
})
export class EmailsComponent implements OnInit {

    emailValido: string;
    anteriorEmail: Email;
    emails: Email[] = [];
    emailsFiltrados: Email[] = [];
    dataSource = new MatTableDataSource(this.emailsFiltrados);
    filtro: string;
    displayedColumns: string[] = ['email', 'edit-delete'];
    @ViewChild(MatSort) sort: MatSort;

    constructor(private _seguridadService:SeguridadService,
                private _toasterService:ToasterService,
                private _cargandoService: CargandoService,
                private _router: Router) { }

    ngOnInit() {
        this._cargandoService.mostrar();
        if (this._seguridadService.usuario.usuario === 'admin') {
            this.Iniciar();
        }
        else {
            this._toasterService.pop({
                type: 'error',
                title: 'Usuario no autorizado para acceder'
            });
            this._cargandoService.ocultar();
            this._router.navigate(['inicio']);
        }
    }

    Iniciar() {
        this.emailValido = undefined;
        this._seguridadService.ConsultarEmails()
            .subscribe( (data:any) => {
                this.emails = data;
                this.emailsFiltrados = this.emails;
                this.dataSource = new MatTableDataSource(this.emailsFiltrados);
                this.dataSource.sort = this.sort;
                this._cargandoService.ocultar();
            });
    }

    filtrar(evento:any) {
        let filtro: string = evento.target.value;
        let exp = new RegExp(/[a-zA-Z0-9._-]{1,}@skycellular.net/);
        filtro = filtro.toLowerCase();
        if (exp.test(filtro)) {
            this.emailValido = filtro;
        }
        else {
            this.emailValido = undefined;
        }
        this.dataSource.filter = filtro.trim().toLowerCase();
        if (evento.key === 'Enter' && this.emailValido && this.dataSource.filteredData.length === 0) {
            this._cargandoService.mostrar();
            this.RegistrarEmail();
        }
    }

    EliminarEmail(idEmail: number) {
        this._cargandoService.mostrar();
        this._seguridadService.EliminarEmail(idEmail)
            .subscribe( (data:any) => {
                if (data) {
                    this._toasterService.pop({
                        type: 'success',
                        title: 'E-mail eliminado'
                    });
                    this.Iniciar()
                }
                else {
                    this._toasterService.pop({
                        type: 'error',
                        title: 'No se pudo eliminar el E-mail'
                    });
                }
                this._cargandoService.ocultar();
            });
    }

    RegistrarEmail() {
        if (this.emailValido) {
            let email = new Email();
            email.email = this.emailValido;
            this._seguridadService.RegistrarEmail(email)
                .subscribe( (data:any) => {
                    if (data) {
                        this._toasterService.pop({
                            type: 'success',
                            title: 'E-mail registrado'
                        });
                        this.filtro = '';
                        this.dataSource.filter = this.filtro.trim().toLowerCase();
                        this.Iniciar();
                    }
                    else {
                        this._toasterService.pop({
                            type: 'error',
                            title: 'Error al registrar el E-mail'
                        });
                        this._cargandoService.ocultar();
                    }
                });
        }
    }

    HabilitarEdicion(email) {
        this.anteriorEmail = Object.assign({}, email);
    }

    CancelarEdicion(email: Email) {
        email.email = this.anteriorEmail.email;
        this.anteriorEmail = undefined;
    }

    ActualizarEmail(email: Email, event: any) {
        let exp = new RegExp(/[a-zA-Z0-9._-]{1,}@skycellular.net/);
        if (event === undefined || event.key === 'Enter') {
            email.email = email.email.toLowerCase();
            if (exp.test(email.email)) {
                this._cargandoService.mostrar();
                this._seguridadService.ActualizarEmail(email)
                .subscribe( (data) => {
                    if (data) {
                        this._toasterService.pop({
                            type: 'success',
                            title: 'El email ha sido actualizado'
                        });
                        this.anteriorEmail = undefined;
                    }
                    else {
                        this._toasterService.pop({
                            type: 'error',
                            title: 'Error al actualizar el E-mail'
                        });
                    }
                    this._cargandoService.ocultar();
                });
            }
            else {
                this._toasterService.pop({
                    type: 'error',
                    title: 'Formato de E-mail no válido'
                });
            }
        }
    }
}
