import { Component, OnInit } from '@angular/core';
import { FormControl, Validators, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { ToasterService } from 'angular2-toaster';
import { SeguridadService } from '../../../../servicios/seguridad.service';
import { ActivatedRoute } from '@angular/router';
import { RolPantalla, Rol, Pantalla } from '../../../../modelos/modelos';
import { CargandoService } from '../../../../servicios/cargando.service';

@Component({
    selector: 'app-rol-detalle',
    templateUrl: './rol-detalle.component.html',
    styles: []
})
export class RolDetalleComponent implements OnInit {

    formulario: FormGroup;
    pantallas: any[] = [];
    pantallasSeleccionadas: RolPantalla[] = [];
    rol: Rol;
    editando: boolean = false;

    constructor(private _seguridadService:SeguridadService,
                private _toasterService:ToasterService,
                private _activatedRoute: ActivatedRoute,
                private _cargandoService: CargandoService,
                private _router: Router) { }

    ngOnInit() {
        this._cargandoService.mostrar();
        this._seguridadService.VerificarPermisos(this._seguridadService.usuario.id, '/roles')
            .subscribe( data => {
                if (data) {
                    this.Iniciar();
                }
                else {
                    this._toasterService.pop({
                        type: 'error',
                        title: 'Usuario no autorizado para acceder'
                    });
                    this._cargandoService.ocultar();
                    this._router.navigate(['inicio']);
                }
            });

    }

    Iniciar() {
        this.formulario = new FormGroup({
            nombre: new FormControl('', Validators.required),
            descripcion: new FormControl('', Validators.required)
        });
        this._seguridadService.ConsultarPantallas()
            .subscribe( (dataPantallas:any) => {
                this.pantallas = dataPantallas;
                this.pantallas.forEach( pantalla => {
                    pantalla.seleccionada = false;
                });
                this._activatedRoute.params.subscribe( params => {
                    if (params['id']) {
                        this.editando = true;
                        this._seguridadService.ConsultarRolPorId(params['id'])
                        .subscribe( (data:any) => {
                            this.rol = data;
                            this.formulario.patchValue(this.rol);
                            data.rolPantallas.forEach( rolPantalla => {
                                let pantalla = this.pantallas.filter( pantalla => { return rolPantalla.id === pantalla.id})[0];
                                pantalla.seleccionada = true;
                            });
                            this._cargandoService.ocultar();
                        });
                    } else {
                        this._cargandoService.ocultar();
                    }
                });
            });
    }
    guardar() {
        if (this.formulario.valid) {
            this._cargandoService.mostrar();
            this.pantallasSeleccionadas = [];
            let pantallasSeleccionadas = this.pantallas.filter( pantalla => {
                return pantalla.seleccionada;
            });
            if (pantallasSeleccionadas.length > 0) {
                pantallasSeleccionadas.forEach( pantalla => {
                    let rolPantalla = new RolPantalla();
                    rolPantalla.pantalla = new Pantalla();
                    rolPantalla.pantalla.id = pantalla.id;
                    if (this.editando) {
                        rolPantalla.rol = new Rol();
                        rolPantalla.rol.id = this.rol.id;
                    }
                    this.pantallasSeleccionadas.push(rolPantalla);
                });
                let rol = {
                    id: this.editando ? this.rol.id : undefined,
                    nombre: this.formulario.value.nombre,
                    descripcion: this.formulario.value.descripcion,
                    rolPantallas: this.pantallasSeleccionadas
                };
                if (this.editando) {
                    this.actualizarRol(rol);
                }
                else {
                    this.crearRol(rol);
                }
            }
            else {
                this._cargandoService.ocultar();
                this._toasterService.pop({
                    type: 'error',
                    title: 'Debe seleccionar algún acceso para el rol'
                });
            }
        }
    }

    crearRol(rol) {
        this._seguridadService.CrearRol(rol)
            .subscribe( data => {
                if (data === true) {
                    this._toasterService.pop({
                        type: 'success',
                        title: 'Rol ' + rol.nombre + ' creado'
                    });
                    this.formulario.reset();
                    this.pantallas.forEach( pantalla => pantalla.seleccionada = false);
                    this.pantallasSeleccionadas = [];
                }
                this._cargandoService.ocultar();
            });
    }

    actualizarRol(rol) {
        this._seguridadService.ActualizarRol(rol)
            .subscribe( data => {
                if (data === true) {
                    this._toasterService.pop({
                        type: 'success',
                        title: 'Rol ' + rol.nombre + ' actualizado'
                    });
                }
                this._cargandoService.ocultar();
            })
    }

}
