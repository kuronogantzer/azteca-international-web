import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { SeguridadService } from '../../../servicios/seguridad.service';
import { CargandoService } from '../../../servicios/cargando.service';
import { Rol } from '../../../modelos/seguridad/Rol';
import { MatSort, MatTableDataSource } from '@angular/material';
import { ToasterService } from 'angular2-toaster';

@Component({
    selector: 'app-roles',
    templateUrl: './roles.component.html',
    styles: []
})
export class RolesComponent implements OnInit {

    roles: Rol[] = [];
    rolesFiltrados: Rol[] = [];
    dataSource = new MatTableDataSource(this.rolesFiltrados);
    filtro: string;
    displayedColumns: string[] = ['nombre', 'descripcion', 'edit-delete'];
    @ViewChild(MatSort) sort: MatSort;

    constructor(private _seguridadService:SeguridadService,
                private _toasterService:ToasterService,
                private _cargandoService: CargandoService,
                private _router: Router) { }

    ngOnInit() {
        this._cargandoService.mostrar();
        this._seguridadService.VerificarPermisos(this._seguridadService.usuario.id, '/roles')
            .subscribe( data => {
                if (data) {
                    this.consultarRoles();
                }
                else {
                    this._toasterService.pop({
                        type: 'error',
                        title: 'Usuario no autorizado para acceder'
                    });
                    this._cargandoService.ocultar();
                    this._router.navigate(['inicio']);
                }
            });
    }

    consultarRoles() {
        this._seguridadService.ConsultarRoles()
            .subscribe( (data:Rol[]) => {
                this.roles = data;
                this.rolesFiltrados = this.roles;
                this.dataSource = new MatTableDataSource(this.rolesFiltrados);
                this.dataSource.sort = this.sort;
                this._cargandoService.ocultar();
            });
    }

    filtrar(filtro:string) {
        this.dataSource.filter = filtro.trim().toLowerCase();
    }

    EliminarRol(id) {
        this._cargandoService.mostrar();
        this._seguridadService.EliminarRol(id)
            .subscribe( (data:any) => {
                if (data === true) {
                    this._toasterService.pop({
                        type: 'success',
                        title: 'Rol eliminado'
                    });
                    this.consultarRoles();
                }
                else {
                    this._cargandoService.ocultar();
                }
            });
    }

}
