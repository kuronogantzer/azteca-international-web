import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormControl, Validators, FormGroup, FormArray } from '@angular/forms';
import { SeguridadService } from '../../../../servicios/seguridad.service';
import { ToasterService } from 'angular2-toaster';
import { Rol, Usuario, TipoIdentificacion, Ciudad, Email} from '../../../../modelos/modelos';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { ActivatedRoute } from '@angular/router';
import { CargandoService } from '../../../../servicios/cargando.service';

@Component({
  selector: 'app-usuario-detalle',
  templateUrl: './usuario-detalle.component.html',
  styles: []
})
export class UsuarioDetalleComponent implements OnInit {

        editando: boolean = false;
        usuario: Usuario;
        datosPrincipalesForm: FormGroup;
        datosContactoForm: FormGroup;
        contrasenaForm: FormGroup;
        TiposDocumento = [];
        nombreUsuario: string;
        ciudades = [];
        ciudadesFiltradas: Observable<string[]>;
        roles: Rol[] = [];
        emails: Email[] =[];

        constructor(private _formBuilder: FormBuilder,
                    private _seguridadService: SeguridadService,
                    private _toasterService: ToasterService,
                    private _activatedRoute: ActivatedRoute,
                    private _cargangoService: CargandoService,
                    private _router: Router) { }

        ngOnInit() {
            this._cargangoService.mostrar();
            this._seguridadService.VerificarPermisos(this._seguridadService.usuario.id, '/usuarios')
                .subscribe( data => {
                    if (data) {
                        this.Iniciar();
                    }
                    else {
                        this._toasterService.pop({
                            type: 'error',
                            title: 'Usuario no autorizado para acceder'
                        });
                        this._cargangoService.ocultar();
                        this._router.navigate(['inicio']);
                    }
                });
        }

        Iniciar() {
            this._seguridadService.ConsultarTiposDocumento()
                .subscribe( (data:any) => {
                    this.TiposDocumento = data;
                });
            this._seguridadService.ConsultarCiudades()
                .subscribe( (data:any) => {
                    this.ciudades = data;
                });
            this._seguridadService.ConsultarRoles()
                .subscribe( (data:any) => {
                    this.roles = data;
                });
            this._seguridadService.ConsultarEmailsPorEstado(0)
                .subscribe( (data:any) => {
                    this.emails = data;
                });
            this.datosPrincipalesForm = this._formBuilder.group({
                primer_nombre: ['', Validators.required],
                segundo_nombre: [''],
                primer_apellido: ['', Validators.required],
                segundo_apellido: [''],
                tipo_identificacion: new FormGroup({
                    id: new FormControl('', Validators.required)
                }),
                numero_identificacion: ['', Validators.required]
            });
            this.datosContactoForm = this._formBuilder.group({
                email: new FormGroup({
                    id: new FormControl('', Validators.required)
                }),
                telefono: ['', [Validators.maxLength(20), Validators.minLength(7)]],
                celular: ['', [Validators.required, Validators.maxLength(20), Validators.minLength(10)]],
                direccion: ['', [Validators.required, Validators.minLength(6)]],
                ciudad:  new FormGroup({
                    nombre: new FormControl('', Validators.required)
                }),
            });
            this.contrasenaForm = this._formBuilder.group({
                //contrasena: ['', [Validators.required, Validators.pattern("(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)([A-Za-z\\d\\W]|[ ]){8,15}$")]],
                rol: new FormGroup({
                    id: new FormControl('', Validators.required)
                }),
                usuario: ['', Validators.required]
            });

            this.ciudadesFiltradas = this.ciudadForm.controls['nombre'].valueChanges
                .pipe(startWith(''), map(value => this.filtrar(value)));
            this._activatedRoute.params.subscribe( params => {
                if (params['id']) {
                    this.editando = true;
                    this._seguridadService.ConsultarUsuarioPorId(params['id'])
                        .subscribe( (data:Usuario) => {
                            this.usuario = data;
                            this.emails.push(this.usuario.email);
                            this.datosPrincipalesForm.patchValue(this.usuario);
                            this.datosContactoForm.patchValue(this.usuario);
                            this.contrasenaForm.patchValue(this.usuario);
                            this.nombreUsuario = this.usuario.usuario;
                            this._cargangoService.ocultar();
                        });
                } else {
                    this._cargangoService.ocultar();
                }
            });
        }

        get ciudadForm() { return <FormArray>this.datosContactoForm.get('ciudad'); }

        filtrar(valor) {
            if (valor) {
                valor = valor.toLowerCase();
                return this.ciudades.filter(ciudad => ciudad.nombre.toLowerCase().includes(valor));
            }
        }

        guardar() {
            if(this.datosPrincipalesForm.valid && this.datosContactoForm.valid && (this.contrasenaForm.valid || this.editando))
            {
                this._cargangoService.mostrar();
                if (!this.editando) {
                    this.usuario = new Usuario();
                    this.usuario.id = undefined;
                    this.usuario.contrasena = this.randomPassword(10);
                    this.usuario.fecha_registro = new Date().toISOString().split('T')[0] + ' ' + new Date().toLocaleTimeString();
                    this.usuario.activo = true;
                    this.usuario.fecha_activacion = new Date().toISOString().split('T')[0] + ' ' + new Date().toLocaleTimeString();
                    this.usuario.fecha_ultimo_ingreso = undefined;
                    this.usuario.fecha_ultima_modificacion = undefined;
                }
                this.usuario.primer_nombre = this.datosPrincipalesForm.value.primer_nombre;
                this.usuario.segundo_nombre = this.datosPrincipalesForm.value.segundo_nombre;
                this.usuario.primer_apellido = this.datosPrincipalesForm.value.primer_apellido;
                this.usuario.segundo_apellido = this.datosPrincipalesForm.value.segundo_apellido;
                this.usuario.tipo_identificacion = new TipoIdentificacion();
                this.usuario.tipo_identificacion.id = this.datosPrincipalesForm.value.tipo_identificacion.id;
                this.usuario.numero_identificacion = this.datosPrincipalesForm.value.numero_identificacion;
                this.usuario.rol = new Rol();
                this.usuario.rol.id = this.contrasenaForm.value.rol.id
                this.usuario.email = this.datosContactoForm.value.email;
                this.usuario.telefono = this.datosContactoForm.value.telefono;
                this.usuario.celular = this.datosContactoForm.value.celular;
                this.usuario.direccion = this.datosContactoForm.value.direccion;
                this.usuario.ciudad = this.ciudades.filter( ciudad => { return ciudad.nombre == this.datosContactoForm.value.ciudad.nombre})[0];
                this.usuario.usuario = this.contrasenaForm.value.usuario;
                if (this.editando) {
                    this.actualizarUsuario();
                }
                else {
                    this.crearUsuario();
                }
            }
        }

        crearUsuario() {
            this._seguridadService.CrearUsuario(this.usuario)
                .subscribe( (data:any) => {
                    if(data === true) {
                        this._toasterService.pop({
                            type: 'success',
                            title: 'Usuario ' + this.usuario.usuario + ' creado'
                        });
                        document.getElementById('resetUserForm').click();
                    }
                    else {
                        console.log(data);
                        this._toasterService.pop({
                            type: 'error',
                            title: data
                        });
                    }
                    this._cargangoService.ocultar();
                }, (err) => {
                    console.log(err);
                    this._toasterService.pop({
                        type: 'error',
                        title: err.error.toString()
                    });
                    this._cargangoService.ocultar();
                });
        }

        actualizarUsuario() {
            this._seguridadService.ActualizarUsuario(this.usuario)
                .subscribe( (data:any) => {
                    if(data === true) {
                        this._toasterService.pop({
                            type: 'success',
                            title: 'Usuario ' + this.nombreUsuario + ' actualizado'
                        });
                    }
                    else {
                        console.log(data);
                        this._toasterService.pop({
                            type: 'error',
                            title: data
                        });
                    }
                    this._cargangoService.ocultar();
                }, (err) => {
                    console.log(err);
                    this._toasterService.pop({
                        type: 'error',
                        title: err.error.toString()
                    });
                    this._cargangoService.ocultar();
                });
        }

        randomPassword(length: number) {
            let chars = "abcdefghijklmnopqrstuvwxyz!@#$%^&*()-+<>ABCDEFGHIJKLMNOP1234567890";
            let pass = "";
            for (let x = 0; x < length; x++) {
                let i = Math.floor(Math.random() * chars.length);
                pass += chars.charAt(i);
            }
            return pass;
        }
}
