import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { SeguridadService } from '../../../servicios/seguridad.service';
import { Usuario } from '../../../modelos/seguridad/Usuario';
import { MatSort, MatTableDataSource } from '@angular/material';
import { ToasterService } from 'angular2-toaster';
import { CargandoService } from '../../../servicios/cargando.service';

@Component({
    selector: 'app-usuarios',
    templateUrl: './usuarios.component.html',
    styles: []
})
export class UsuariosComponent implements OnInit {

    usuarios: Usuario[] = [];
    usuariosFiltrados = [];
    dataSource = new MatTableDataSource(this.usuariosFiltrados);
    filtro: string;
    displayedColumns: string[] = ['nombre_completo', 'usuario', 'email', 'celular', 'edit-delete'];
    @ViewChild(MatSort) sort: MatSort;

    constructor(private _seguridadService:SeguridadService,
        private _toasterService:ToasterService,
        private _cargangoService: CargandoService,
        private _router: Router) { }

        ngOnInit() {
            this._cargangoService.mostrar();
            this._seguridadService.VerificarPermisos(this._seguridadService.usuario.id, '/usuarios')
            .subscribe( data => {
                if (data) {
                    this.consultarUsuarios();
                }
                else {
                    this._toasterService.pop({
                        type: 'error',
                        title: 'Usuario no autorizado para acceder'
                    });
                    this._cargangoService.ocultar();
                    this._router.navigate(['inicio']);
                }
            });
        }

        consultarUsuarios() {
            this._seguridadService.ConsultarUsuarios()
            .subscribe( (data:Usuario[]) => {
                this.usuarios = data;
                this.usuariosFiltrados = this.usuarios;
                this.usuariosFiltrados.forEach( usuario => {
                    usuario.nombre_completo = usuario.primer_nombre + ' ' +
                    usuario.segundo_nombre + ' ' +
                    usuario.primer_apellido + ' ' +
                    usuario.segundo_apellido;
                });
                this.dataSource = new MatTableDataSource(this.usuariosFiltrados);
                this.dataSource.filterPredicate = (data, filter: string)  => {
                    const accumulator = (currentTerm, key) => {
                        return this.nestedFilterCheck(currentTerm, data, key);
                    };
                    const dataStr = Object.keys(data).reduce(accumulator, '').toLowerCase();
                    const transformedFilter = filter.trim().toLowerCase();
                    return dataStr.indexOf(transformedFilter) !== -1;
                };
                this.dataSource.sort = this.sort;
                this._cargangoService.ocultar()
            });
        }

        nestedFilterCheck(search, data, key) {
            if (typeof data[key] === 'object') {
                for (const k in data[key]) {
                    if (data[key][k] !== null) {
                        search = this.nestedFilterCheck(search, data[key], k);
                    }
                }
            } else {
                search += data[key];
            }
            return search;
        }

        filtrar(filtro:string) {
            this.dataSource.filter = filtro.trim().toLowerCase();
        }

        EliminarUsuario(id) {
            this._cargangoService.mostrar();
            this._seguridadService.EliminarUsuario(id)
            .subscribe( (data:any) => {
                if (data === true) {
                    this._toasterService.pop({
                        type: 'success',
                        title: 'Usuario eliminado'
                    });
                    this.consultarUsuarios();
                }
            })
        }
    }
