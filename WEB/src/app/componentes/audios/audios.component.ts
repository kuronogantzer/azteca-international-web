import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToasterService } from 'angular2-toaster';
import { SeguridadService } from '../../servicios/seguridad.service';

@Component({
      selector: 'app-audios',
      templateUrl: './audios.component.html',
      styles: []
})
export class AudiosComponent implements OnInit {

      constructor(private _seguridadService: SeguridadService,
                  private _router: Router,
                  private _toasterService:ToasterService,) { }

      ngOnInit() {
          this._seguridadService.VerificarPermisos(this._seguridadService.usuario.id, '/audios')
              .subscribe( data => {
                  if (data) {
                      this.Iniciar();
                  }
                  else {
                      this._toasterService.pop({
                          type: 'error',
                          title: 'Usuario no autorizado para acceder'
                      });
                      this._router.navigate(['inicio']);
                  }
              });
      }

      Iniciar() {}

}
