export class Pantalla {
    id: number;
    nombre: string;
    descripcion: string;
    ruta: string;
    opcion_padre: string;
}
