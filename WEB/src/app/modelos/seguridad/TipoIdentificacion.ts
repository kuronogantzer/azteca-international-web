export interface ITipoIdentificacion {
    id: number;
    codigo: string;
    descripcion: string;
}

export class TipoIdentificacion implements ITipoIdentificacion {
    id: number;
    codigo: string;
    descripcion: string;
}
