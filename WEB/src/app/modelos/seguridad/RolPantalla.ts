import { Rol } from './Rol';
import { Pantalla } from './Pantalla';

export class RolPantalla {
    id: number;
    rol: Rol;
    pantalla: Pantalla;
}
