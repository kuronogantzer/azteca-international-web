import { TipoIdentificacion } from './TipoIdentificacion';
import { Ciudad } from './Ciudad';
import { Rol } from './Rol';
import { Email } from './Email';

export class Usuario {
    id: number;
    tipo_identificacion: TipoIdentificacion;
    numero_identificacion: string;
    primer_nombre: string;
    segundo_nombre: string;
    primer_apellido: string;
    segundo_apellido: string;
    usuario: string;
    rol: Rol;
    email: Email;
    contrasena: string;
    telefono: string;
    celular: string;
    direccion: string;
    ciudad: Ciudad;
    fecha_registro: string;
    activo: boolean;
    fecha_activacion: string;
    fecha_ultimo_ingreso: Date;
    fecha_ultima_modificacion: Date;
}
