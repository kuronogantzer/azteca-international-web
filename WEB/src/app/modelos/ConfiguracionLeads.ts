import { EstadoCodigo } from './EstadoCodigo';

export class ConfiguracionLeads {
    id: number;
    descripcion: string;
    activa: boolean;
    estadosCodigos: EstadoCodigo[];
}
