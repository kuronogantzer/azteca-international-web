export class EstadoCodigo {
    id: number;
    zona: number;
    estado: string;
    ciudad: string;
    codigo: string;
}
