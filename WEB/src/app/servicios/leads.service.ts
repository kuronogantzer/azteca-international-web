import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
// import { Observable, Subject } from 'rxjs';
// import { WebsocketService } from './websocket.service';
// import { environment } from '../../environments/environment';
// import { Usuario } from '../modelos/seguridad/usuario';
// import { RolPantalla } from '../modelos/seguridad/RolPantalla';
import { Socket } from 'ngx-socket-io';
import { ConfiguracionLeads } from '../modelos/ConfiguracionLeads';

@Injectable({
    providedIn: 'root'
})
export class LeadsService {

    private BaseUrl = "http://localhost:5000/";
    public message: any;

    constructor(private _http:HttpClient
    /*private socket: Socket*/) {
    }

    ConsultarEstadosCodigos() {
        return this._http.get(this.BaseUrl + "EstadosCodigos");
    }

    GenerarLeads(codigosSeleccionados) {
        return this._http.post(this.BaseUrl + "GenerarLeads", codigosSeleccionados);
    }

    ConsultarConfiguracionesLeads() {
        return this._http.get(this.BaseUrl + "ConfiguracionLeads");
    }

    ConsultarConfiguracionLeads(idConfiguracionLeads: number) {
        return this._http.get(this.BaseUrl + "ConfiguracionLeads/" + idConfiguracionLeads.toString());
    }

    CrearConfiguracionLeads(configuracionLeads: ConfiguracionLeads) {
        return this._http.post(this.BaseUrl + "ConfiguracionLeads", configuracionLeads);
    }

    ActualizarConfiguracionLeads(configuracionLeads: ConfiguracionLeads) {
        return this._http.put(this.BaseUrl + "ConfiguracionLeads", configuracionLeads);
    }

    EliminarConfiguracionLeads(idConfiguracionLeads: number) {
        return this._http.delete(this.BaseUrl + "ConfiguracionLeads/" + idConfiguracionLeads.toString());
    }

    CambiarEstadoConfiguracionLeads(idConfiguracionLeads: number, estado:boolean) {
        return this._http.get(this.BaseUrl + "CambiarEstadoConfiguracionLeads/" + idConfiguracionLeads.toString());
    }

    // GenerarLeads(codigosSeleccionados){
    //     this.socket.emit("GenerarLeads", codigosSeleccionados);
    // }

    // Respuesta() {
    //     return this.socket.fromEvent("Respuesta");
    // }
}
