import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Usuario, RolPantalla, Email } from '../modelos/modelos';
import { map } from 'rxjs/operators';
import * as jwt_decode from 'jwt-decode';

@Injectable({
    providedIn: 'root'
})
export class SeguridadService {

    private BaseUrl = "http://localhost:5000/";
    usuario: Usuario;

    constructor(private _http:HttpClient) {

    }

    ValidarToken() {
        let usuario = JSON.parse(localStorage.getItem('currentUser'));
        let token = '';
        if (usuario) {
            token = usuario.token
        } else {
            return false;
        }
        const decoded = jwt_decode(token);
        if (decoded.exp === undefined) {
            return false;
        }
        const date = new Date(0);
        date.setUTCSeconds(decoded.exp);
        if(date === undefined) {
            return false;
        }
        return (date.valueOf() > new Date().valueOf());
    }

    ActualizarPermisos() {
        let usuario = JSON.parse(localStorage.getItem('currentUser'));
        if (usuario) {
            this.usuario = usuario.usuario;
            return usuario.rolesPantallas.map( (rolPantalla) => rolPantalla.pantalla );
        }
        return undefined;
    }

    RefrescarToken(idUsuario) {
        return this._http.post(this.BaseUrl + "Token", { idUsuario: idUsuario });
    }

    Login(credenciales:any) {
        return this._http.post<any>(this.BaseUrl + "Login", credenciales)
            .pipe(map(user => {
                    if (user && user.token) {
                        localStorage.setItem('currentUser', JSON.stringify(user));
                    }
                    return user;
                }));
    }

    Logout() {
        let usuario: any = JSON.parse(localStorage.getItem('currentUser'));
        localStorage.removeItem('currentUser');
        return this._http.delete(this.BaseUrl + "Token/" + usuario.usuario.id.toString());
    }

    ConsultarUsuarios() {
        //this.httpOptions.headers['Authorization'] = this.token;
        return this._http.get(this.BaseUrl + "Usuario");
    }

    ConsultarUsuarioPorId(idUsuario) {
        return this._http.get(this.BaseUrl + "Usuario/" + idUsuario.toString());
    }

    ActualizarUsuario(usuario:Usuario) {
        return this._http.put(this.BaseUrl + "Usuario", usuario);
    }

    CrearUsuario(usuario:Usuario) {
        return this._http.post(this.BaseUrl + "Usuario", usuario);
    }

    EliminarUsuario(idUsuario) {
        return this._http.delete(this.BaseUrl + "Usuario/" + idUsuario.toString());
    }

    ConsultarRoles() {
        return this._http.get(this.BaseUrl + "Rol");
    }

    ConsultarRolPorId(id) {
        return this._http.get(this.BaseUrl + "Rol/" + id.toString());
    }

    CrearRol(rol) {
        return this._http.post(this.BaseUrl + "Rol", rol);
    }

    ActualizarRol(rolPantalla:RolPantalla) {
        return this._http.put(this.BaseUrl + "Rol", rolPantalla);
    }

    EliminarRol(id) {
        return this._http.delete(this.BaseUrl + "Rol/" + id.toString());
    }

    ConsultarPantallas() {
        return this._http.get(this.BaseUrl + "Pantalla");
    }

    ConsultarTiposDocumento() {
        return this._http.get(this.BaseUrl + "TipoDocumento");
    }

    ConsultarCiudades() {
        return this._http.get(this.BaseUrl + "Ciudad");
    }

    VerificarPermisos(idUsuario: number, ruta: string) {
        let datos = { idUsuario: idUsuario, ruta: ruta };
        return this._http.post(this.BaseUrl +"Permisos", datos);
    }

    ConsultarEmails() {
        return this._http.get(this.BaseUrl + "Email");
    }

    ConsultarEmailsPorEstado(estado: number) {
        return this._http.get(this.BaseUrl + "Email/" + estado.toString());
    }

    RegistrarEmail(email: Email) {
        return this._http.post(this.BaseUrl + "Email", email);
    }

    ActualizarEmail(email: Email) {
        return this._http.put(this.BaseUrl + "Email", email);
    }

    EliminarEmail(idEmail: number) {
        return this._http.delete(this.BaseUrl + "Email/" + idEmail.toString());
    }
}
