import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/index';

import { EstadoCargando } from '../modelos/cargando';

@Injectable({
    providedIn: 'root'
})
export class CargandoService {

    private cargandoSubject = new Subject<EstadoCargando>();

    estadoCargando = this.cargandoSubject.asObservable();

    constructor() { }

    mostrar() {
        this.cargandoSubject.next(<EstadoCargando>{mostrar: true});
    }

    ocultar() {
        this.cargandoSubject.next(<EstadoCargando>{mostrar: false});
    }
}
